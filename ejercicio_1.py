
'''
Iterar la lista 'abuelo' y mostrar la información de la siguiente manera:
-------Padre 1 ---------
    ---Hijo 1------
        *10
        *20
        *30
    ---Hijo 2------
        *40
        *50
        *60
-------Padre 2 ---------
    ---Hijo 1------
        *10
        *20
        *30
        *40
    ---Hijo 2------
        *10
        *20
        *30
        *40
'''

abuelo = [
    [[10, 20, 30], [40, 50, 60], [20, 10, 80]],
    [[70, 20, 80], [100, 150, 160], [120, 140, 10]],
    [[5, 8, 15], [50, 40, 90], [110, 115, 105]]
]

# Solución de Jesus David
""" for padre in abuelo:
    print(f"-------Padre {abuelo.index(padre)+1}---------")
    for hijo in padre:
        print(f"----Hijo {padre.index(hijo) + 1}------")
        for h in hijo:
            print(h)
 """

for padre in abuelo:
    sumatoria = 0
    for hijo in padre:
        sumatoria += sum(hijo)
    print(sumatoria)
