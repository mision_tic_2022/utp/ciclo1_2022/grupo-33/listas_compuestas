'''
Desarrolle una función que reciba como parámetro la lista compuesta 'notas',
Retornar el promedio por cada lista padre y  el promedio general dentro de una tupla:
    ([4.5, 4.2], 4.35)
'''
notas = [
    [
        [4.5, 3.2, 4.9, 4.2],
        [3.5, 4.2, 3.9, 4.8]
    ],
    [
        [4.9, 3.9, 4.9, 3.2],
        [3.8, 4.4, 2.9, 3.8]
    ]
]

# Solución de Brayan Esteban


def promedio(n):
    counterSalon = 0
    for result in n:
        counterSalon += 1
        print(f'------------------Salon {counterSalon}---------------')
        counterEstudiante = 0
        for nota in result:
            counterEstudiante += 1
            print(f'Estudiante {counterEstudiante}')
            notas = 0
            counter = 0
            for num in nota:
                notas = notas+num
                counter += 1
            resultado = notas/counter
            print(
                f'El promedio de Estudiante {counterEstudiante} es: {resultado}')


#promedio(notas)

notas = [
    [
        [4.5, 3.2, 4.9, 4.2],
        [3.5, 4.2, 3.9, 4.8]
    ],
    [
        [4.9, 3.9, 4.9, 3.2],
        [3.8, 4.4, 2.9, 3.8]
    ]
]

def calcular_promedios(notas: list)->tuple:
    promedio_notas_x_salon = []
    for salon in notas:
        promedio_salon = 0
        cant_notas = 0
        for estudiante in salon:
            promedio_salon += sum(estudiante)
            cant_notas += len(estudiante)
        #Calcular el promedio
        promedio_salon /=  cant_notas
        #Añadir promedio a la lista
        promedio_notas_x_salon.append(promedio_salon)
    #Calcular promedio general
    promedio_general = sum(promedio_notas_x_salon) / len(promedio_notas_x_salon)
    #Retornar tupla con los promedios
    return (promedio_notas_x_salon, promedio_general)

#promedio_salones, promedio_general = calcular_promedios(notas)
print(calcular_promedios(notas))
