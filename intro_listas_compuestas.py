
hijo_1 = ['María', 'Juan']
hijo_2 = ['Alejandra', 'Luis']

padre = [hijo_1, hijo_2, ['Andres', 'Dario'], ['Milfer', 'Antonio']]

nombre_1 = padre[0][0]
print(nombre_1)

nombre_2 = padre[0][1]
print(nombre_2)

nombre_3 = padre[1][0]
print(nombre_3)

nombre_4 = padre[1][1]
print(nombre_4)

print('---------------------')

#Recorrer listas compuestas
for hijo in padre:
    print(hijo)
    #Iterar lista hijo
    for j in hijo:
        print(j)
